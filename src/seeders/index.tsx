import { v4 as uuidv4 } from 'uuid';
import D1 from 'app/pages/HomePage/assets/d1.png';
import D2 from 'app/pages/HomePage/assets/d2.png';
import D3 from 'app/pages/HomePage/assets/d3.png';
import D4 from 'app/pages/HomePage/assets/d4.png';
import D5 from 'app/pages/HomePage/assets/d5.png';
import D6 from 'app/pages/HomePage/assets/d6.png';
import D7 from 'app/pages/HomePage/assets/d7.png';
import D8 from 'app/pages/HomePage/assets/d8.png';

import Record1 from 'app/pages/MyRecordPage/assets/r1.png';
import Record2 from 'app/pages/MyRecordPage/assets/r2.png';
import Record3 from 'app/pages/MyRecordPage/assets/r3.png';

import C1 from 'app/pages/RecommendedPage/assets/img1.png';
import C2 from 'app/pages/RecommendedPage/assets/img2.png';
import C3 from 'app/pages/RecommendedPage/assets/img3.png';
import C4 from 'app/pages/RecommendedPage/assets/img4.png';
import C5 from 'app/pages/RecommendedPage/assets/img5.png';
import C6 from 'app/pages/RecommendedPage/assets/img6.png';
import C7 from 'app/pages/RecommendedPage/assets/img7.png';
import C8 from 'app/pages/RecommendedPage/assets/img8.png';

export const listDishFaker = () => {
  return [
    {
      uuid: uuidv4(),
      image: D1,
      type: '05.21.Morning',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D2,
      type: '05.21.Lunch',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D3,
      type: '05.21.Dinner',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D4,
      type: '05.21.Snack',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D5,
      type: '05.20.Morning',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D6,
      type: '05.20.Lunch',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D7,
      type: '05.20.Lunch',
      quality: 100,
    },
    {
      uuid: uuidv4(),
      image: D8,
      type: '05.20.Lunch',
      quality: 100,
    },
  ];
};

export const myRecordsFaker = () => {
  return [
    {
      uuid: uuidv4(),
      title: 'BODY RECORD',
      btn: '自分のカラダの記録',
      bg: Record1,
    },
    {
      uuid: uuidv4(),
      title: 'MY EXERCISE',
      btn: '自分の運動の記録',
      bg: Record2,
    },
    { uuid: uuidv4(), title: 'MY DIARY', btn: '自分の日記', bg: Record3 },
  ];
};

export const listRecommendFaker = () => {
  return [
    {
      uuid: uuidv4(),
      image: C1,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C2,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C3,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C4,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C5,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C6,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C7,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
    {
      uuid: uuidv4(),
      image: C8,
      content:
        '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ 魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ',
      hashTag: ['#魚料理', '#和食', '#DHA'],
      createdAt: '2022-10-10T17:33:02.379Z',
    },
  ];
};
