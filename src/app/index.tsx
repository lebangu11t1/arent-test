/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './pages/HomePage/Loadable';
import { MyRecordPage } from './pages/MyRecordPage/Loadable';
import { RecommendedPage } from './pages/RecommendedPage/Loadable';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { useTranslation } from 'react-i18next';

// load react bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

import { Layout } from 'app/components/Layout/Loadable';

export function App() {
  const { i18n } = useTranslation();
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - React Boilerplate"
        defaultTitle="React Boilerplate"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A React Boilerplate application" />
      </Helmet>

      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/my-record" element={<MyRecordPage />} />
          <Route path="/recommended" element={<RecommendedPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Layout>

      <GlobalStyle />
    </BrowserRouter>
  );
}
