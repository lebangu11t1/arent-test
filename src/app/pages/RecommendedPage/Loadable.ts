/**
 *
 * Asynchronously loads the component for RecommendedPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const RecommendedPage = lazyLoad(
  () => import('./index'),
  module => module.RecommendedPage,
);
