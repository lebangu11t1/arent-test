import * as React from 'react';
import { render } from '@testing-library/react';

import { RecommendedPage } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<RecommendedPage  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<RecommendedPage />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
