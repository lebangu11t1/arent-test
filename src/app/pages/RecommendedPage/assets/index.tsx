import MainPhoto from './main_photo.png';
import MainPhotoText from './main_photo_text.png';
import Rhombus from './rhombus.png';

export { MainPhoto, MainPhotoText, Rhombus };
