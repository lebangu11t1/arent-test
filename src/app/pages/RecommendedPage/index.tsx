import { Helmet } from 'react-helmet-async';
import {
  Categories,
  ListRecommend,
} from 'app/pages/RecommendedPage/components';
import styled from 'styled-components/macro';

export function RecommendedPage() {
  return (
    <>
      <Helmet>
        <title>Healthy care home page</title>
        <meta name="description" content="Healthy care home page" />
      </Helmet>

      <Div>
        <Categories />
        <ListRecommend />
      </Div>
    </>
  );
}

const Div = styled.div`
  margin: 56px 0 64px 0;
`;
