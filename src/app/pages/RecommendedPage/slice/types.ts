/* --- STATE --- */
export interface RecommendedPageSagaState {
  recommended: RecommendedItemType[];
}

export interface RecommendedItemType {
  uuid: string;
  image: string;
  content: string;
  hashTag: string[];
  createdAt: string;
}
