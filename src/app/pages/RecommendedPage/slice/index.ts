import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { recommendedPageSaga } from './saga';
import { RecommendedPageSagaState } from './types';

export const initialState: RecommendedPageSagaState = {
  recommended: [],
};

const slice = createSlice({
  name: 'recommendedPage',
  initialState,
  reducers: {
    getRecommended(state, action: PayloadAction<any>) {
      // dispatch fetch api
    },
    getRecommendedSuccess(state, action: PayloadAction<any>) {
      state.recommended = action.payload;
    },
    getRecommendedFailed(state, action: PayloadAction<any>) {
      // do something here with response errors
    },
  },
});

export const { actions: recommendedPageActions } = slice;

export const useRecommendedPageSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: recommendedPageSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useHomePageSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
