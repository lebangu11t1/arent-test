import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.recommendedPage || initialState;

export const selectRecommendedPageSaga = createSelector(
  [selectSlice],
  state => state,
);

export const selectListRecommend = createSelector(
  [selectSlice],
  state => state.recommended,
);
