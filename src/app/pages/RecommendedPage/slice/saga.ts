import { put, takeLatest } from 'redux-saga/effects';
import { recommendedPageActions as actions } from '.';
import { listRecommendFaker } from 'seeders';

function* getRecommend() {
  try {
    // call API
    const res = listRecommendFaker();
    yield put(actions.getRecommendedSuccess(res));
  } catch (error) {
    yield put(actions.getRecommendedFailed(error));
  } finally {
    // final loading
  }
}

export function* recommendedPageSaga() {
  yield takeLatest(actions.getRecommended.type, getRecommend);
}
