/**
 *
 * Categories
 *
 */
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';

interface Props {}

export const Categories = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Wrapper>
      <div className="category">
        <h2>RECOMMENDED COLUMN</h2>
        <div className="line" />
        <p>オススメ</p>
      </div>
      <div className="category">
        <h2>RECOMMENDED DIET</h2>
        <div className="line" />
        <p>ダイエット</p>
      </div>
      <div className="category">
        <h2>RECOMMENDED BEAUTY</h2>
        <div className="line" />
        <p>美容</p>
      </div>
      <div className="category">
        <h2>RECOMMENDED HEALTH</h2>
        <div className="line" />
        <p>健康</p>
      </div>
    </Wrapper>
  );
});

const Wrapper = styled.div`
  --column: 4;
  --spacing: 32px;
  width: calc(100% + var(--spacing));
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-left: calc(-1 * var(--spacing));
  margin-bottom: 56px;

  .category {
    width: calc(calc(100% / var(--column)) - var(--spacing));
    margin-left: var(--spacing);
    height: 144px;
    background: #2e2e2e;
    text-align: center;
    padding: 24px 8px 0 8px;

    /* &:first-child {
      margin-right: 0;
    } */

    h2 {
      width: 200px;
      font-family: 'Inter';
      font-weight: 400;
      font-size: 22px;
      line-height: 27px;
      margin: 0 auto 10px;
      color: #ffcc21;
    }

    .line {
      width: 56px;
      height: 1px;
      margin: 0 auto;
      background: #ffffff;
      margin-bottom: 8px;
    }

    p {
      font-style: normal;
      font-weight: 300;
      font-size: 18px;
      line-height: 26px;
      text-align: center;
      color: #ffffff;
    }
  }
`;
