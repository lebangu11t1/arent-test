/**
 *
 * ListDish
 *
 */
import { ButtonArent } from 'app/components/ButtonArent';
import { memo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/macro';
import { useRecommendedPageSlice } from '../../slice';
import { selectListRecommend } from '../../slice/selectors';
import { RecommendItem } from './components';

interface Props {}

export const ListRecommend = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const ListRecommend = useSelector(selectListRecommend);
  const dispatch = useDispatch();
  const { actions } = useRecommendedPageSlice();

  useEffect(() => {
    dispatch(actions.getRecommended({}));
  }, [actions, dispatch]);

  return (
    <Div>
      <WrapperListRecommend>
        {ListRecommend.map(recommend => (
          <COL key={recommend.uuid}>
            <RecommendItem recommand={recommend} />
          </COL>
        ))}
      </WrapperListRecommend>
      <Loadmore>
        <ButtonArent label="コラムをもっと見る" />
      </Loadmore>
    </Div>
  );
});

const Div = styled.div``;

const WrapperListRecommend = styled.div`
  --column: 4;
  --spacing: 8px;
  width: calc(100% + var(--spacing));
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-left: calc(-1 * var(--spacing));
`;

const COL = styled.div`
  width: calc(calc(100% / var(--column)) - var(--spacing));
  margin-left: var(--spacing);
  margin-bottom: 18px;
`;

const Loadmore = styled.div`
  text-align: center;
  margin-top: 6px;
  padding-bottom: 28px;
`;
