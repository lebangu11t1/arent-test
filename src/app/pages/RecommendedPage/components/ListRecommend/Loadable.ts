/**
 *
 * Asynchronously loads the component for ListDish
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ListRecommend = lazyLoad(
  () => import('./index'),
  module => module.ListRecommend,
);
