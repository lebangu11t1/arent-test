/**
 *
 * Asynchronously loads the component for DishItem
 *
 */

import { lazyLoad } from 'utils/loadable';

export const RecommendItem = lazyLoad(
  () => import('./index'),
  module => module.RecommendItem,
);
