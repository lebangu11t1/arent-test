/**
 *
 * DishItem
 *
 */
import { formatDateTime } from 'app/constants';
import React, { memo } from 'react';
import styled from 'styled-components/macro';

interface Props {
  recommand: any;
}

export const RecommendItem = memo((props: Props) => {
  const { image, uuid, content, hashTag, createdAt } = props.recommand;

  return (
    <Div>
      <WrapperImage>
        <Image src={image} alt={uuid} />
        <Time>{formatDateTime(createdAt, 'YYYY-MM-DD hh:mm')}</Time>
      </WrapperImage>
      <Content>{content}</Content>
      <HashTag>
        {hashTag.map((item, index) => (
          <span key={index}>{item}</span>
        ))}
      </HashTag>
    </Div>
  );
});

const Div = styled.div`
  width: 100%;
  margin-bottom: 8px;
`;

const WrapperImage = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 100%;
  height: 100%;
`;

const Time = styled.div`
  padding: 3px 8px;
  font-family: 'Inter';
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 24px;
  color: #ffffff;
  background: #ffcc21;

  position: absolute;
  bottom: 0;
`;

const Content = styled.p`
  height: 48px;
  margin-top: 5px;
  margin-bottom: 0;
  font-style: normal;
  font-weight: 300;
  font-size: 15px;
  line-height: 22px;
  color: #414141;

  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

const HashTag = styled.div`
  font-style: normal;
  font-weight: 300;
  font-size: 12px;
  line-height: 22px;

  color: #ff963c;

  span {
    margin-right: 10px;
  }
`;
