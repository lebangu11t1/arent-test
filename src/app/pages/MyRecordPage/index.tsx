/**
 *
 * MyRecordPage
 *
 */
import {
  BodyRecord,
  MyDiary,
  MyExercise,
  MyRecords,
} from 'app/pages/MyRecordPage/components';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';

interface Props {}

export const MyRecordPage = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Div>
      <MyRecords />
      <BodyRecord />
      <MyExercise />
      <MyDiary />
    </Div>
  );
});

const Div = styled.div`
  padding-top: 3.5rem;
`;
