/**
 *
 * MyRecords
 *
 */
import { memo } from 'react';
import { Col, Row } from 'react-bootstrap';
import { myRecordsFaker } from 'seeders';
import { RecordItem } from '../RecordItem/Loadable';

interface Props {}

export const MyRecords = memo((props: Props) => {
  return (
    <Row>
      {myRecordsFaker().map((record: any) => (
        <Col sm={4} md={4} xs={4} key={record.uuid}>
          <RecordItem record={record} />
        </Col>
      ))}
    </Row>
  );
});
