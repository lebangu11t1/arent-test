/**
 *
 * Asynchronously loads the component for MyRecords
 *
 */

import { lazyLoad } from 'utils/loadable';

export const MyRecords = lazyLoad(
  () => import('./index'),
  module => module.MyRecords,
);
