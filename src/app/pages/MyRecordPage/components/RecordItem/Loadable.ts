/**
 *
 * Asynchronously loads the component for RecordItem
 *
 */

import { lazyLoad } from 'utils/loadable';

export const RecordItem = lazyLoad(
  () => import('./index'),
  module => module.RecordItem,
);
