/**
 *
 * RecordItem
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';

interface Props {
  record: any;
}

export const RecordItem = memo(({ record }: Props) => {
  const { title, btn, bg } = record;
  return (
    <Div bg={bg}>
      <div>
        <Title>{title}</Title>
        <Text>{btn}</Text>
      </div>
    </Div>
  );
});

const Div = styled.div<{
  bg?: string;
}>`
  width: 100%;
  height: 288px;

  background: #2e2e2e;
  border: 24px solid #ffcc21;

  background-image: url(${props => props.bg});
  background-repeat: no-repeat;
  background-size: 100% 100%;
  backdrop-filter: none;
  image-rendering: pixelated;

  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.div`
  font-style: normal;
  font-weight: 400;
  font-size: 25px;
  line-height: 30px;
  text-align: center;
  letter-spacing: 0.125px;
  color: #ffcc21;
  padding-bottom: 10px;
`;

const Text = styled.div`
  font-family: 'Hiragino Kaku Gothic Pro';
  font-style: normal;
  font-weight: 300;
  font-size: 14px;
  line-height: 20px;
  /* identical to box height, or 143% */

  text-align: center;
  color: #ffffff;

  width: 160px;
  height: 20px;
  background-color: #ff963c;
`;
