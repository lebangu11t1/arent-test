import { MyRecords } from './MyRecords/Loadable';
import { BodyRecord } from './BodyRecord/Loadable';
import { MyExercise } from './MyExercise/Loadable';
import { MyDiary } from './MyDiary/Loadable';

export { MyRecords, BodyRecord, MyExercise, MyDiary };
