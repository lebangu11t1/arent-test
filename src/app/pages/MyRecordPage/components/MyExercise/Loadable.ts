/**
 *
 * Asynchronously loads the component for MyExercise
 *
 */

import { lazyLoad } from 'utils/loadable';

export const MyExercise = lazyLoad(
  () => import('./index'),
  module => module.MyExercise,
);
