/**
 *
 * MyExercise
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';
import { Row, Col } from 'react-bootstrap';

interface Props {}

const ExerciseItem = () => (
  <Col sm={6} md={6} xs={6}>
    <div className="item-ex">
      <div className="item-ex__info">
        <div className="item-ex__circle"></div>
        <div>
          <div>家事全般（立位・軽い）</div>
          <span>26kcal</span>
        </div>
      </div>
      <div className="item-ex__time">10 min</div>
    </div>
  </Col>
);

export const MyExercise = memo((props: Props) => {
  return (
    <Div id="scrollbar">
      <div className="top-ex">
        <div className="h-title">
          <p>My</p> EXERCISE
        </div>
        <span>2021.05.21</span>
      </div>

      <Row>
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
        <ExerciseItem />
      </Row>
    </Div>
  );
});

const Div = styled.div`
  width: 100%;
  height: 264px;
  padding-left: 24px;
  padding-top: 16px;
  background: #414141;

  font-size: 15px;
  letter-spacing: 0.15px;
  color: #ffffff;

  border: 1px solid #ccc;
  margin-bottom: 56px;

  overflow: auto;
  overflow-x: hidden;

  /* width */
  ::-webkit-scrollbar {
    width: 3px;
    background: #777777;
    border-radius: 3px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #777777;
    border-radius: 3px;
  }

  ::-webkit-scrollbar-thumb {
    background: #ffcc21;
    border-radius: 3px;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #ffcc21;
    border-radius: 3px;
  }

  .top-ex {
    display: flex;
    padding-bottom: 1rem;
    p {
      margin-bottom: 0;
    }
    span {
      font-size: 22px;
    }
  }

  .h-title {
    padding-right: 2rem;
  }

  .item-ex {
    width: 100%;
    height: 48px;
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #777777;
    padding-right: 2rem;

    &__circle {
      width: 5px;
      height: 5px;
      border-radius: 50%;
      background-color: #fff;
      margin-right: 5px;
    }

    &__info {
      display: flex;
      align-items: baseline;
    }

    &__time {
      font-weight: 400;
      font-size: 18px;
      letter-spacing: 0.09px;
      color: #ffcc21;
    }

    span {
      font-weight: 400;
      font-size: 15px;
      color: #ffcc21;
      letter-spacing: 0.075px;
      font-weight: 400;
    }
  }
`;
