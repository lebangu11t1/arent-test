/**
 *
 * MyDiary
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';
import { Row, Col } from 'react-bootstrap';
import { ButtonArent } from 'app/components/ButtonArent';

const DiaryItem = () => (
  <Col sm={3} md={3} xs={3} className="pr-0">
    <Diary>
      <div className="mydiary__date">2021.05.21</div>
      <p className="mydiary__date">23:25</p>

      <span>私の日記の記録が一部表示されます。</span>
      <p>
        キストテキストテキストテキストテキストテキストテキストテキストテキストテ
        キストテキストテキストテキストテキストテキストテテキストテキストテキスト
        テキストテキストテキストテキストテキキストテキストテキスト…
      </p>
    </Diary>
  </Col>
);

interface Props {}

export const MyDiary = memo((props: Props) => {
  return (
    <Div className="mydiary">
      <h4 className="mydiary__title">MY DIARY</h4>
      <Row>
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
        <DiaryItem />
      </Row>

      <WrapperBtn>
        <ButtonArent label="自分の日記をもっと見る" />
      </WrapperBtn>
    </Div>
  );
});

const Div = styled.div`
  .mydiary {
    &__title {
      font-family: 'Inter';
      font-style: normal;
      font-weight: 400;
      font-size: 22px;
      line-height: 27px;
      letter-spacing: 0.11px;
      color: #414141;
    }

    &__date {
      font-family: 'Inter';
      font-style: normal;
      font-weight: 400;
      font-size: 18px;
      line-height: 22px;
      letter-spacing: 0.09px;
    }
  }
`;

const Diary = styled.div`
  /* 長方形 27 (custom style) */
  height: 231px;
  width: 100%;
  background: #ffffff;
  border: 2px solid #707070;
  padding: 16px;
  margin-bottom: 12px;

  font-family: 'Hiragino Kaku Gothic Pro';
  font-style: normal;
  font-weight: 300;
  font-size: 12px;
  line-height: 17px;
  letter-spacing: 0.06px;

  color: #414141;
`;

const WrapperBtn = styled.div`
  margin-top: 20px;
  margin-bottom: 30px;

  display: flex;
  align-items: center;
  justify-content: center;
`;
