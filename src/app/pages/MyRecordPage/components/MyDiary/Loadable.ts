/**
 *
 * Asynchronously loads the component for MyDiary
 *
 */

import { lazyLoad } from 'utils/loadable';

export const MyDiary = lazyLoad(
  () => import('./index'),
  module => module.MyDiary,
);
