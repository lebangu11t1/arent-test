/**
 *
 * BodyRecord
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';

import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from 'chart.js';
import { faker } from '@faker-js/faker';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
);

const labels = [
  '6月',
  '7月',
  '8月',
  '9月',
  '10月',
  '11月',
  '12月',
  '1月',
  '2月',
  '3月',
  '4月',
  '5月',
];

export const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: false,
    },
  },
};

export const data = {
  labels,
  datasets: [
    {
      data: labels.map(() => faker.datatype.number({ min: 100, max: 500 })),
      borderColor: 'rgb(255, 204, 33)',
      backgroundColor: 'rgba(255, 204, 33, 0.5)',
    },
    {
      data: labels.map(() => faker.datatype.number({ min: 100, max: 500 })),
      borderColor: 'rgb(143, 233, 208)',
      backgroundColor: 'rgba(143, 233, 208, 0.5)',
    },
  ],
};

interface Props {}

export const BodyRecord = memo((props: Props) => {
  return (
    <Div>
      <LineChart>
        <Line id="line-chart" options={options} data={data} />
      </LineChart>
    </Div>
  );
});

const Div = styled.div`
  width: 100%;
  height: 304px;

  margin-top: 56px;
  margin-bottom: 56px;
`;

const LineChart = styled.div`
  #line-chart {
    background-color: #2e2e2e;
    width: 100% !important;
    height: 316px !important;
  }
`;
