/**
 *
 * Asynchronously loads the component for BodyRecord
 *
 */

import { lazyLoad } from 'utils/loadable';

export const BodyRecord = lazyLoad(
  () => import('./index'),
  module => module.BodyRecord,
);
