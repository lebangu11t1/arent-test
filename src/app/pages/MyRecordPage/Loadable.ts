/**
 *
 * Asynchronously loads the component for MyRecordPage
 *
 */

import { lazyLoad } from 'utils/loadable';

export const MyRecordPage = lazyLoad(
  () => import('./index'),
  module => module.MyRecordPage,
);
