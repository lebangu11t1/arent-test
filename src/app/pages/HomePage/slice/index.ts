import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { homePageSaga } from './saga';
import { HomePageState } from './types';

export const initialState: HomePageState = {
  dishes: [],
};

const slice = createSlice({
  name: 'homePage',
  initialState,
  reducers: {
    getDishes(state, action: PayloadAction<any>) {
      // dispatch fetch api
    },
    getDishesSuccess(state, action: PayloadAction<any>) {
      state.dishes = action.payload;
    },
    getDishesFailed(state, action: PayloadAction<any>) {
      // do something here with response errors
    },
  },
});

export const { actions: homePageActions } = slice;

export const useHomePageSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: homePageSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useHomePageSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
