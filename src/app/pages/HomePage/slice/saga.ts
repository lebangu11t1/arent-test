import { put, takeLatest } from 'redux-saga/effects';
import { homePageActions as actions } from '.';
import { listDishFaker } from 'seeders';

function* getDishes() {
  try {
    // call API
    const res = listDishFaker();
    yield put(actions.getDishesSuccess(res));
  } catch (error) {
    yield put(actions.getDishesFailed(error));
  } finally {
    // final loading
  }
}

export function* homePageSaga() {
  yield takeLatest(actions.getDishes.type, getDishes);
}
