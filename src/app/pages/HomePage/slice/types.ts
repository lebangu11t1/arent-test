/* --- STATE --- */
export interface HomePageState {
  dishes: DishItemType[];
}

export interface DishItemType {
  uuid: string;
  image: string;
  type: string;
  quality: number;
}
