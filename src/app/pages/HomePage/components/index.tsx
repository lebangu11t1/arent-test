import { PhotoAndGraph } from './PhotoAndGraph/Loadable';
import { Categories } from './Categories/Loadable';
import { ListDish } from './ListDish/Loadable';

export { PhotoAndGraph, Categories, ListDish };
