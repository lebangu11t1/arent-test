/**
 *
 * Asynchronously loads the component for ListDish
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ListDish = lazyLoad(
  () => import('./index'),
  module => module.ListDish,
);
