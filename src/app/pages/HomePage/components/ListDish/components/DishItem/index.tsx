/**
 *
 * DishItem
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';

interface Props {
  dish: any;
}

export const DishItem = memo((props: Props) => {
  const { image, uuid } = props.dish;

  return (
    <Div>
      <Image src={image} alt={uuid} />
    </Div>
  );
});

const Div = styled.div`
  width: 100%;
  height: 234px;
  margin-bottom: 8px;
  /* border: 1px solid black; */
`;

const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: fill;
`;
