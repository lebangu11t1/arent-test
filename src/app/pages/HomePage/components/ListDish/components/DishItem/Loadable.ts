/**
 *
 * Asynchronously loads the component for DishItem
 *
 */

import { lazyLoad } from 'utils/loadable';

export const DishItem = lazyLoad(
  () => import('./index'),
  module => module.DishItem,
);
