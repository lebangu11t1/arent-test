/**
 *
 * ListDish
 *
 */
import { ButtonArent } from 'app/components/ButtonArent';
import { useHomePageSlice } from 'app/pages/HomePage/slice';
import { selectListDish } from 'app/pages/HomePage/slice/selectors';
import { memo, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/macro';
import { DishItem } from './components';

interface Props {}

export const ListDish = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  const listDish = useSelector(selectListDish);
  const dispatch = useDispatch();
  const { actions } = useHomePageSlice();

  useEffect(() => {
    dispatch(actions.getDishes({}));
  }, [actions, dispatch]);

  return (
    <Div>
      <Row>
        {listDish.map(dish => (
          <COL key={dish.uuid} sm={3}>
            <DishItem dish={dish} />
          </COL>
        ))}
      </Row>
      <Loadmore>
        <ButtonArent label="記録をもっと見る" />
      </Loadmore>
    </Div>
  );
});

const Div = styled.div``;

const COL = styled(Col)`
  padding: 0;
  padding-right: 8px;
`;

const Loadmore = styled.div`
  text-align: center;
  margin-top: 20px;
  padding-bottom: 28px;
`;
