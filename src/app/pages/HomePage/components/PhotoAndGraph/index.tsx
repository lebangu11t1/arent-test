/**
 *
 * PhotoAndGraph
 *
 */
import { MainPhoto, MainPhotoText } from 'app/pages/HomePage/assets';
import { memo } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';

import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from 'chart.js';
import { faker } from '@faker-js/faker';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
);

const labels = [
  '6月',
  '7月',
  '8月',
  '9月',
  '10月',
  '11月',
  '12月',
  '1月',
  '2月',
  '3月',
  '4月',
  '5月',
];

export const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: false,
    },
  },
};

export const data = {
  labels,
  datasets: [
    {
      data: labels.map(() => faker.datatype.number({ min: 100, max: 500 })),
      borderColor: 'rgb(255, 204, 33)',
      backgroundColor: 'rgba(255, 204, 33, 0.5)',
    },
    {
      data: labels.map(() => faker.datatype.number({ min: 100, max: 500 })),
      borderColor: 'rgb(143, 233, 208)',
      backgroundColor: 'rgba(143, 233, 208, 0.5)',
    },
  ],
};

interface Props {}

export const PhotoAndGraph = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Wrapper className="clearfix">
      <Row>
        <Col sm={5} className="pr-0">
          <Banner>
            <img
              width="180"
              height="180"
              className="img-text"
              src={MainPhotoText}
              alt="main-text"
            />
          </Banner>
        </Col>
        <Col sm={7} className="pl-0">
          <LineChart>
            <Line id="line-chart" options={options} data={data} />
          </LineChart>
        </Col>
      </Row>
    </Wrapper>
  );
});

const Wrapper = styled.div`
  width: 100vw;
  margin-left: calc(50% - 50vw);
  height: 316px;
`;

const Banner = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  background-image: url(${MainPhoto});
  background-repeat: no-repeat;
  background-size: 100% 100%;
`;

const LineChart = styled.div`
  #line-chart {
    background-color: #2e2e2e;
    width: 100% !important;
    height: 316px !important;
  }
`;
