/**
 *
 * Asynchronously loads the component for PhotoAndGraph
 *
 */

import { lazyLoad } from 'utils/loadable';

export const PhotoAndGraph = lazyLoad(
  () => import('./index'),
  module => module.PhotoAndGraph,
);
