/**
 *
 * Categories
 *
 */
import { CupIcon, KnifeIcon } from 'app/components/Images';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';
import { Rhombus } from '../../assets';

interface Props {}

export const Categories = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Wrapper>
      <div className="hexagon">
        <KnifeIcon />
        <p>Morning</p>
      </div>
      <div className="hexagon">
        <KnifeIcon />
        <p>Lunch</p>
      </div>
      <div className="hexagon">
        <KnifeIcon />
        <p>Dinner</p>
      </div>
      <div className="hexagon">
        <CupIcon />
        <p>Snack</p>
      </div>
    </Wrapper>
  );
});

const Wrapper = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 25px 0 25px 0;

  .hexagon {
    width: 134px;
    height: 116px;

    background-image: url(${Rhombus});
    background-repeat: no-repeat;
    background-size: 100% 100%;

    text-align: center;
    display: inline-block;
    padding-top: 1rem;

    font-style: normal;
    font-weight: 200;
    font-size: 1.25rem;
    color: #ffffff;
  }
`;
