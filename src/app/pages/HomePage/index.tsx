import { Helmet } from 'react-helmet-async';
import {
  PhotoAndGraph,
  Categories,
  ListDish,
} from 'app/pages/HomePage/components';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Healthy care home page</title>
        <meta name="description" content="Healthy care home page" />
      </Helmet>

      <>
        <PhotoAndGraph />
        <Categories />
        <ListDish />
      </>
    </>
  );
}
