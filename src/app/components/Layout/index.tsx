/**
 *
 * Layout
 *
 */
import { Footer } from 'app/components/Footer/Loadable';
import { NavBars } from 'app/components/NavBars/Loadable';
import { memo } from 'react';
import { Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';

interface Props {
  children: any;
}

export const Layout = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <>
      <NavBars />
      <Wrapper>{props.children}</Wrapper>
      <Footer />
    </>
  );
});

const Wrapper = styled(Container)`
  /* border: solid 1px red; */
`;
