/**
 *
 * Asynchronously loads the component for NavBars
 *
 */

import { lazyLoad } from 'utils/loadable';

export const NavBars = lazyLoad(
  () => import('./index'),
  module => module.NavBars,
);
