import {
  ChallengeIcon,
  InfoIcon,
  LogoIcon,
  MenuIcon,
  MyRecordIcon,
} from 'app/components/Images';
import { Col, Container, Row } from 'react-bootstrap';
import styled from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';

export function NavBars() {
  return (
    <Wrapper>
      <Container>
        <Row>
          <Col>
            <LogoIcon />
          </Col>

          <Menus>
            <div>
              <MyRecordIcon />
              <span className="m-title">自分の記録</span>
            </div>
            <div>
              <ChallengeIcon />
              <span className="m-title">チャレンジ</span>
            </div>
            <div>
              <InfoIcon />
              <span className="m-title">お知らせ</span>
            </div>
            <div>
              <MenuIcon />
            </div>
          </Menus>
        </Row>
      </Container>
    </Wrapper>
  );
}

const Wrapper = styled.header`
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.160784);

  width: 100%;
  height: ${StyleConstants.NAV_BAR_HEIGHT};
  background-color: #414141;
  color: #ffffff;

  display: flex;
  position: fixed;
  top: 0;
  z-index: 2;

  @supports (backdrop-filter: blur(10px)) {
    backdrop-filter: blur(10px);
    background-color: #414141;
  }
`;

const Menus = styled(Col)`
  display: flex;
  align-items: center;
  justify-content: space-between;

  .m-title {
    margin-left: 0.5rem;
  }
`;
