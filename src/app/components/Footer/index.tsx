/**
 *
 * Footer
 *
 */
import { memo } from 'react';
import { Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';

interface Props {}

export const Footer = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Div>
      <Container className="pl-0">
        <span>会員登録</span>
        <span>運営会社</span>
        <span>利用規約</span>
        <span>個人情報の取扱について</span>
        <span>特定商取引法に基づく表記</span>
        <span>お問い合わせ</span>
      </Container>
    </Div>
  );
});

const Div = styled.div`
  width: 100%;
  height: 128px;
  background: #414141;

  display: flex;
  align-items: center;

  span {
    font-family: 'Hiragino Kaku Gothic Pro';
    font-style: normal;
    font-weight: 300;
    font-size: 11px;
    line-height: 16px;
    /* identical to box height, or 145% */
    letter-spacing: 0.033px;
    color: #ffffff;
    padding-right: 2.813rem;
  }
`;
