import LogoIcon from './LogoIcon';
import MyRecordIcon from './MyRecordIcon';
import ChallengeIcon from './ChallengeIcon';
import InfoIcon from './InfoIcon';
import MenuIcon from './MenuIcon';
import KnifeIcon from './KnifeIcon';
import CupIcon from './CupIcon';
import Hexagon from './Hexagon';

export {
  LogoIcon,
  MyRecordIcon,
  ChallengeIcon,
  InfoIcon,
  MenuIcon,
  KnifeIcon,
  CupIcon,
  Hexagon,
};
