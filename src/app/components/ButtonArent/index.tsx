/**
 *
 * Button
 *
 */
import React, { memo } from 'react';
import styled from 'styled-components/macro';
import Button from 'react-bootstrap/Button';

interface Props {
  label: string;
}

export const ButtonArent = memo((props: Props) => {
  return (
    <>
      <BtnArent className="btn-primary" variant="primary">
        {props.label}
      </BtnArent>
    </>
  );
});

const BtnArent = styled(Button)`
  width: 296px;
  height: 56px;
  background: linear-gradient(32.95deg, #ffcc21 8.75%, #ff963c 86.64%);
  border: none;
`;
