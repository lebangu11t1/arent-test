import moment from 'moment';

export const formatDateTime = (createdAt, format) => {
  return moment(createdAt).format(format);
};
